package org.craigtreptow

import org.scalatest.funspec.AnyFunSpec

class CubeCalculatorTest extends AnyFunSpec {
  describe("Something") {
    describe("when something") {
      it("should work") {
        assert(CubeCalculator.cube(4) === 64)
      }
    }
  }
}
