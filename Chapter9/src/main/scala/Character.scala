package org.craigtreptow

case class Character(name: String, isThief: Boolean)
