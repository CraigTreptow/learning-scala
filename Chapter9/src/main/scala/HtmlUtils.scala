package org.craigtreptow

trait HtmlUtils {
def removeMarkup(input: String) = {
  input
    .replaceAll("""</?\w[^>]*>""","")
    .replaceAll("<.*>","")
}
}
