package org.craigtreptow

object Exercises extends App {
  val h = Character("Hadrian", true)
  val r = h.copy(name = "Royce")

  println(h)
  println(r)
  println(h == r)
  val is_thief = h match {
    case Character(x, true) => s"$x is a thief"
    case Character(x, false) => s"$x is not a thief"
  }
  println(is_thief)
  val html = "<html><body><h1>Introduction</h1></body></html>"
  val strippedHtml = new Page(html).asPlainText
  println(s"$html --> $strippedHtml")
  println(new Page("").asPlainText)
  println(new Page(null).asPlainText)
}
