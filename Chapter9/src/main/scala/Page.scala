package org.craigtreptow

class Page(val s: String) extends SafeStringUtils with HtmlUtils {
  def asPlainText: String = { trimToNone(s) map removeMarkup getOrElse "N/A" }
}
