name := "Chapter9"

version := "0.1"

scalaVersion := "2.13.4"

idePackagePrefix := Some("org.craigtreptow")

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.2" % Test
