object Exercises extends App {
  // Exercie #1
  // Centigrade to Fahrenheit conversion

  def cToF(degreesC: Float): Float = {
    (degreesC * 9/5) + 32
  }

  println("Exercis #1")
  println(cToF(0))   // 32.0
  println(cToF(-1))  // 30.2
  println(cToF(100)) // 212.0

  // Exercise #2
  // Same as #1, but return an Int

  def cToFInt(degreesC: Float): Int = {
    (degreesC * 9/5).toInt + 32
  }

  println("\nExercis #2")
  println(cToFInt(0))   // 32
  println(cToFInt(-1))  // 31
  println(cToFInt(100)) // 212

  // Exercise #3
  // String interpolation

  def interpolateThis(n: Double): String = {
    // source: https://alvinalexander.com/scala/how-to-format-numbers-commas-international-currency-in-scala/

    val formatter = java.text.NumberFormat.getCurrencyInstance
    val formattedN = formatter.format(n)
    s"You owe ${formattedN}."

    // There may be a way to get this to work, but why??
    // f"You owe ${n}%'.2f"
    // "$%'.2f\n"
  }

  println("\nExercis #3")
  println(interpolateThis(2.7255))

  // Exercise #4
  // Is there a simpler way?

  def simplifyThis(): Unit = {
    val flag: Boolean = false
    val result: Boolean = (flag == false)

    // Simpler:
    // flag == false
  }

  println("\nExercis #4")

  // Exercise #5
  // Convert between types

  def convertThis(n: Int): Int = {
    val ch  = n
    val str = ch.toString
    val dbl = str.toDouble
    val int = str.toInt

    int
  }

  println("\nExercis #5")
  println(convertThis(128))

  // Exercise #6
  // RE strings

  def matchThis(input: String): String = {
    val pattern = """.*(\d{3})-(\d{3})-(\d{4}).*""".r
    val pattern(prefix, areaCode, phoneNumber) = input

    (prefix, areaCode, phoneNumber).toString
  }

  println("\nExercis #6")
  println(matchThis("Frank,123 Main,925-555-1943,95122"))

}
