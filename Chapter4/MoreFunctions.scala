object MoreFunctions extends App {
  // This will be an error
  //def bad_identity(a: Any): Any = a
  //val s: String = bad_identity("Hello")

  // The type is also used as the return type here
  def identity[A](a: A): A = a

  val s: String = identity[String]("Hello")
  println(s)

  val d: Double = identity[Double](2.717)
  println(d)

  // with type inference, we can just do this
  // I think I prefer the explicit type signatures right now, though.
  val dd = identity(2.717)

  val ss = "vacation.jpg"
  val isJPEG = ss.endsWith("jpg")
  println(isJPEG)

  /**
   * Returns the input string without leading or trailing
   * whitespace, or null if the input string is null.
   * @param s the input string to trim, or null.
  */
   def safeTrim(s: String): String = {
     if (s == null) return null
     s.trim()
   }

   // scaladoc *.scala produced some .html and .js source
   // files as documentation.  Pretty good looking, too.
}
