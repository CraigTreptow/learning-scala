object Exercises extends App {
  // exercise #1
  // calculate are of circle, given radius
  def areaOfCircle(radius: Double): Double = {
    scala.math.Pi * (radius * radius)
  }

  println("exercise #1")
  println(areaOfCircle(3.4))

  // exercise #2
  // calculate are of circle, given radius as a string
  def areaOfCircle(radius: String): Double = {
    def calcArea(radius: Double): Double = {
      scala.math.Pi * (radius * radius)
    }

    val result = radius match {
      case "" => calcArea(0.0)
      case s  => calcArea(s.toDouble)
    }

    result
  }

  println("\nexercise #2")
  println(areaOfCircle("3.4"))
  println(areaOfCircle(""))

  // exercise #3
  // print values from 5 to 50 by 5 using recursion
  // make tail recursive if possible
  def countByFives(n: Int):Unit = {
    if (n >= 50) {
      println(s"${n}")
      return
    }
    println(s"${n}")
    countByFives(n + 5)
  }

  println("\nexercise #3")
  countByFives(5)

  // exercise #4
  // given milliseconds, return a string describing the value
  // in days, hours, minutes, and seconds
  def millisecondsToWords(milliseconds: Long): String = {
    val seconds = milliseconds / 1000
    val minutes = seconds / 60
    val hours   = minutes / 60
    val days    = hours / 24

    s"Days: ${days} Hours: ${hours} Minutes: ${minutes} Seconds: ${seconds}"
  }

  // exercise #5
  // calculate first arg raised to the power of the second
  // use math.pow
  def pow(x: Int, y: Int): Double = {
    scala.math.pow(x, y)
  }

  println("\nexercise #5")
  println( pow(2,3) ) // 8.0
  println( pow(3,2) ) // 9.0

  // exercise #5b
  // calculate first arg raised to the power of the second
  // don't use math.pow
  // skipping, as I just can't find this interesting enough

  // exercise #6
  // calc difference between a pair of 2D points and return the result as point
  def pointDiff(p1: Tuple2[Double, Double], p2: Tuple2[Double, Double]): Tuple2[Double, Double] = {
    val diffA = p1._1 - p2._1
    val diffB = p1._2 - p2._2

    (
      scala.math.round(diffA * 100.0) / 100.0,
      scala.math.round(diffB * 100.0) / 100.0
    )
  }

  val p1 = Tuple2(1.1, 2.2)
  val p2 = Tuple2(1.5, 2.5)

  println("\nexercise #6")
  println( pointDiff(p1, p2) )

  // exercise #7
  // input is Tuple3, return a Tuple6, with each param and it's string
  // representation
  def t3ToT6[A,B,C](t: Tuple3[A,B,C]):Tuple6[A,String,B,String,C,String] = {
    (
      t._1, t._1.toString,
      t._2, t._2.toString,
      t._3, t._3.toString
    )
  }

  def t3ToT6V2[A,B,C](t: Tuple3[A,B,C]):Tuple6[A,String,B,String,C,String] = t match {
    case (a,b,c) => (a, a.toString, b, b.toString, c, c.toString)
  }

  // def <function-name>[type-name](parameter-name>: <type-name>): <type-name>
  // my attempt
  // def t3T555oT6[Tuple3[A, B, C]](t: Tuple3[A, B, C]) = ???

  // book answer
  def stringify[A,B,C](t: (A,B,C)): (A,String,B,String,C,String) = {
    (t._1, t._1.toString, t._2, t._2.toString, t._3, t._3.toString)
  }

  println("\nexercise #7")
  println( t3ToT6( (true, 22.5, "yes") ) )
  println( t3ToT6V2( (true, 22.5, "yes") ) )
}
