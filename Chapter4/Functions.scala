object Functions extends App {
  @annotation.tailrec
  def power(x: Int, n: Int, t: Int = 1): Long = {
    if (n < 1)
      t
    else
      power(x, n-1, x * t)
  }

  println(power(2, 8))

  // totally legit, but I'd probably rename the nested function
  def max(a: Int, b: Int, c: Int) = {
    def max(x: Int, y: Int) = if (x > y) x else y

    max(a, max(b, c))
  }

  println(max(1,89,23))

  // named params with defaults is a thing!!
  def greet(name: String, prefix: String = "") = s"$prefix $name"

  println(greet("Craig", "Mr."))
  println(greet("Lisa"))

  // I think I'd probably use an array like sum2 below
  // This is called a vararg to match one or more parameters
  def sum1(items: Int*): Int = {
    var total = 0
    for (i <- items) total += i
    total
  }

  println(sum1(10, 20, 30))

  def sum2(items: Array[Int]): Int = {
    var total = 0
    for (i <- items) total += i
    total
  }

  println(sum2(Array(10, 20, 30)))

  // Parameter Groups are a thing and apparently, will be more useful
  // later when we get to Higher Order Functions
  def max2(x: Int)(y: Int) = if (x > y) x else y
  println( max2(89)(23) )
}
