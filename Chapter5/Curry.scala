object Curry extends App {
  def factorOf1(x: Int, y: Int) = y % x == 0

  val f = factorOf1 _
  val x = f(7, 20)

  println(x)

  // is 3 a factor of ?
  // this is a partially applied function
  val multipleOf3 = factorOf1(3, _: Int)
  val y = multipleOf3(78)

  println(y)

  // cleaner way than the 1st version
  //
  // known as currying
  def factorOf2(x: Int)(y: Int) = y % x == 0
  val isEven = factorOf2(2) _

  // 2 parameter lists
  val z  = factorOf2(2)(32)
  val zz = isEven(32)

  println(z)
  println(zz)

  // by-name parameters
  // references x (the function twice)
  // so we see two lines of outtput from the f function
  def doubles(x: => Int) = {
    println("Now doubling " + x)
    x * 2
  }

  doubles(5)

  def f(i: Int) = { println(s"Hello from f($i)"); i }
  doubles( f(8) )
}
