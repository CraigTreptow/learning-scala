object Exercises extends App {
  // exercise #1
  // Write a function literal that takes two integers and returns the higher number.
  // Then write a higher-order function that takes a 3-sized tuple of integers
  // plus this function literal, and uses it to return the maximum value in the tuple.

  val biggest = (x: Int, y: Int) => if (x > y) x else y

  def maxxer(nums: (Int, Int, Int), f: (Int, Int) => Int): Int = {
    f(f(nums._1, nums._2), nums._3)
  }

  println("exercise #1")
  println(biggest(3,4))
  println(biggest(5,4))
  println(maxxer((1,2,3), biggest))
  println(maxxer((6,7,3), biggest))

  // exercise #2
  // The library function util.Random.nextInt returns a random integer.
  // Use it to invoke the “max” function with two random integers plus a function
  // that returns the larger of two given integers.
  // Do the same with a function that returns the smaller of two given integers,
  // and then a function that returns the second integer every time.

  val smallest = (x: Int, y: Int) => if (x < y) x else y
  val second = (x: Int, y: Int) => y

  def gimme(a: Int, b: Int, f: (Int, Int) => Int): Int = {
    f(a, b)
  }

  println("\nexercise #2")
  val a = util.Random.nextInt()
  val b = util.Random.nextInt()
  println(a, b)
  println(gimme(a,b, biggest))
  println(gimme(a,b, smallest))
  println(gimme(a,b, second))

  // exercise #3
  // Write a higher-order function that takes an integer and returns a function.
  // The returned function should take a single integer argument (say, “x”)
  // and return the product of x and the integer passed to the higher-order function.

  def makeNewF(a: Int): Int => Int = {
    val f = (x: Int) => { a * x }
    f
  }

  println("\nexercise #3")
  val g = makeNewF(2)
  println(g(3))

  // exercise #4
  // Let’s say that you happened to run across this function while reviewing
  // another developer’s code:
  //
  // def fzero[A](x: A)(f: A => Unit): A = { f(x); x }
  //
  // What does this function accomplish?
  // Can you give an example of how you might invoke it?

  def fzero[A](x: A)(f: A => Unit): A = { f(x); x }

  // It takes an x of some type A, and a function of type A to Unit
  // then runs the function on x and returns x
  // So, I guess it's purpose is to induce side-effects.

  println("\nexercise #4")
  def doubles(x: => Int) = { x * 2 }
  val answer = fzero[Int](3) { _ * 2 }
  println(answer)


  // exercise #5
  // There’s a function named “square” that you would like to store in a
  // function value.  Is this the right way to do it?
  // How else can you store a function in a value?

  def square(m: Double) = m * m
  // val sq = square

  val sq = square _

  println("\nexercise #5")
  println(sq(4))

  // exercise #6
  // Write a function called “conditional” that takes a value x and
  // two functions, p and f, and returns a value of the same type as x.
  //
  // The p function is a predicate, taking the value x and returning a Boolean b.
  // The f function also takes the value x and returns a new value of the same type.
  // Your “conditional” function should only invoke the function f(x)
  // if p(x) is true, and otherwise return x.
  //
  // How many type parameters will the “conditional” function require?

  def conditional[A](x: A, p: (A) => Boolean, f: (A) => A) = {
    if (p(x) == true) {
      f(x)
    }
    else {
      x
    }
  }

  val pp = (x: Int) => if (x < 5) true else false
  val ff = (x: Int) => x * 5

  val answer6= conditional[Int](4, pp, ff)

  println("\nexercise #6")
  println(answer6)

  // exercise #7
  // Use #6 to implement FizzBuzz

  // val pp = (x: Int) => if (x < 5) true else false
  val isTypeSafe = (x: Int) => if ((x % 3 == 0) && (x % 5 == 0)) true else false
  val isSafe     = (x: Int) => if (x % 5 == 0) true else false
  val isType     = (x: Int) => if (x % 3 == 0) true else false
  val ts = (x: Int) => -1
  val s  = (x: Int) => -2
  val t  = (x: Int) => -3

  for (x <- 1 to 100) {
    x match {
      case x if conditional[Int](x, isTypeSafe, ts) == -1 => println("typesafe")
      case x if conditional[Int](x, isSafe, s)      == -2 => println("safe")
      case x if conditional[Int](x, isType, t)      == -3 => println("type")
      case x                                              => println(x)
    }
  }
}
