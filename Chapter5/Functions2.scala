object Functions2 extends App {
  // function literal
  //            _________________
  val doubler = (x: Int) => x * 2
  val doubled = doubler(22)
  println( doubled)

  // function literals are also known as:
  // anonymous functions
  // lambda expressions
  // lambdas

  val greeter = (name: String) => s"Hello, $name"
  val hi = greeter("World")
  println(hi)

  // example of multiple params
  def max(a: Int, b: Int) = if (a > b) a else b
  val maximize1: (Int, Int) => Int = max

  val maximize2 = (a: Int, b: Int) => if (a > b) a else b

  println( maximize1(50, 30) )
  println( maximize2(50, 30) )

  // in most cases, I think I'd prefer that logStart be a 
  // defined funtion so can more easily reuse it
  def logStart() = "=" * 50 + "\nStarting NOW\n" + "=" * 50
  val start1: () => String = logStart
  val start2 = () => "=" * 50 + "\nStarting NOW\n" + "=" * 50

  println( start1() )
  println( start2() )

  def safeStringOp(s: String, f: String => String) = {
    if (s != null) f(s) else s
  }

  // can also do wacky stuff like this
  // defining the fuction literal within the HO function invocations
  safeStringOp(null, (s: String) => s.reverse)
}
