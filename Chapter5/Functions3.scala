object Functions3 extends App {
  // placeholder syntax
  val doubler: Int => Int = _ * 2

  def safeStringOp(s: String, f: String => String) = {
    if (s != null) f(s) else s
  }

  // Can do when:
  // Type is specified outside of the literal definition
  // Params only used once
  println( safeStringOp(null,    _.reverse) )
  println( safeStringOp("Craig", _.reverse) )

  def combination(x: Int, y: Int, f: (Int,Int) => Int) = f(x,y)
  println( combination(23, 12, _ * _) )

  def tripleOp1(a: Int, b: Int, c: Int, f: (Int, Int, Int) => Int) = f(a,b,c)
  println( tripleOp1(23, 92, 14, _ * _ + _) )

  // generic using type parameters
  def tripleOp2[A,B](a: A, b: A, c: A, f: (A, A, A) => B) = f(a,b,c)

  println( tripleOp2[Int,Int](23, 92, 14, _ * _ + _) )
  println( tripleOp2[Int,Double](23, 92, 14, 1.0 * _ / _ / _) )
  println( tripleOp2[Int,Boolean](93, 92, 14, _ > _ + _) )

}
