object Functions extends App {
  def double(x: Int): Int = x * 2

  // function type Int => Int
  // means it takes an Int and returns an Int
  // using the result of the 'double' function

  // this is a value, except you can also invoke it
  // the explicit type distinguishes this as a function value
  // and not a function invocation
  val myDouble: (Int) => Int = double

  // functions with a single param can leave off the ()
  val myDouble2 = double _

  println( myDouble(5) )
  println( myDouble2(5) )

  // example of multiple params
  def max(a: Int, b: Int) = if (a > b) a else b
  val maximize: (Int, Int) => Int = max

  println( maximize(50, 30) )

  // or no params
  def logStart() = "=" * 50 + "\nStarting NOW\n" + "=" * 50
  val start: () => String = logStart

  println( start() )

  // good use of higher order functions
  // calling other functions that act on strings,
  // but only if the stirng is not null
  def reverser(s: String) = s.reverse
  def safeStringOp(s: String, f: String => String) = {
    if (s != null) f(s) else s
  }

  println( safeStringOp("Craig", reverser) )
  println( safeStringOp(null,    reverser) )
}
