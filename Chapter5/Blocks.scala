object Blocks extends App {
  def safeStringOp1(s: String, f: String => String) = {
    if (s != null) f(s) else s
  }

  val uuid = java.util.UUID.randomUUID.toString

  val timedUUID1 = safeStringOp1(uuid, { s =>
    val now = System.currentTimeMillis
    val timed = s.take(24) + now
    timed.toUpperCase
  })

  // "cleaner" version below
  def safeStringOp2(s: String)(f: String => String) = {
    if (s != null) f(s) else s
  }

  val timedUUID2 = safeStringOp2(uuid) { s =>
    val now = System.currentTimeMillis
    val timed = s.take(24) + now
    timed.toUpperCase
  }

  println(timedUUID1)
  println(timedUUID2)


  // functions that wrap code blocks in "utilities"
  // Uses: wrap in DB transactions, retries, conditional ENV, etc.
  def timer[A](f: => A): A = {
    def now = System.currentTimeMillis
    val start = now; val a = f; val end = now
    println(s"Executed in ${end - start} ms")
    a
  }

  val veryRandomAmount = timer {
    util.Random.setSeed(System.currentTimeMillis)
    for (i <- 1 to 100000) util.Random.nextDouble
    util.Random.nextDouble
  }

  println(veryRandomAmount)
}
