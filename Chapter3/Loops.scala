object Loops extends App {
  for (x <- 1 to 7) { println(s"Day $x:") }

  val more_days = for (x <- 1 to 7) yield { s"Another Day $x:" }
  // curly braces are not necessary with a single line expression
  for (day <- more_days) print(day + ", ")
  println("")

  // iterator guards == filter
  val threes = for (i <- 1 to 20 if i % 3 == 0) yield i
  println(threes)

  val quote = "Faith,Hope,,Charity"
  for { t <- quote.split(",")
        if t != null
        if t.size > 0
  } { println(t) }

  for { x <- 1 to 2
        y <- 1 to 3
  } { print(s"($x,$y) ") }
  println("")

  // value binding
  val powersOf2 = for (i <- 0 to 8; pow = 1 << i) yield pow
  println(powersOf2)
}
