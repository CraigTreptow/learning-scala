object Exercises extends App {
  // Exercie #1
  // return "n/a" if empty
  // return same string if nonempty
  def nameOrNot(name: String): String = {
    name match {
      case n if n== null => "n/a"
      case n if n== ""   => "n/a"
      case n             => n
    }
  }

  println("Exercise #1")
  println(nameOrNot("bob"))
  println(nameOrNot(null))
  println(nameOrNot(""))

  // Exercise #2
  // With pattern matching, I don't think I'd
  // ever do this with if/then statements
  def greaterOrNot(num: Double): String = {
    num match {
      case n if n > 0  => "greater"
      case n if n == 0 => "same"
      case _           => "less"
    }
  }

  println("\nExercise #2")
  println(greaterOrNot(45))
  println(greaterOrNot(0))
  println(greaterOrNot(-10))

  // Exercise #3
  // Convert cyan, magenta, or yellow to their six-char hexadecimal
  // equivalents in string form

  def toHexForm(color: String): String = {
    color.toLowerCase match {
      case "cyan"    => "00ffff"
      case "magenta" => "ff00ff"
      case "yellow"  => "ffff00"
      case c         => s"unhandled color: ${c}"
    }
  }

  println("\nExercise #3")
  println(toHexForm("craig"))
  println(toHexForm("cyan"))
  println(toHexForm("Magenta"))

  // Exercise #4
  // print numbers 1 to 100, with each line containing a group of five
  println("\nExercise #4")
  for (x <- 1 to 100) {if (x % 5 == 0) println(x) else print(x + ", ")}

  // Exercise #5
  // fizzbuzz, but with "type" and "safe"
  println("\nExercise #5")
  for (x <- 1 to 100) {
    x match {
      case x if ((x % 3 == 0) && (x % 5 == 0)) => println("typesafe")
      case x if (x % 5 == 0)                   => println("safe")
      case x if (x % 3 == 0)                   => println("type")
      case x                                   => println(x)
    }
  }

  // Exercise #6
  // I'm skipping, because I don't care about code golf
  // However, I will put here an improved version (or at least different)
  println("\nExercise #6")
  1 to 100 foreach { n =>
    println((n % 3, n % 5) match {
      case (0, 0) => "typesafe"
      case (_, 0) => "safe"
      case (0, _) => "type"
      case _      => n
    })
  }
}
