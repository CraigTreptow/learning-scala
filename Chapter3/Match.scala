object Match extends App {
  val x = 10
  val y = 20

  val max1 = if (x > y) x else y

  val max2 = x > y match {
    case true => x
    case false => y
  }

  // pattern alternative
  val day1 = "MON"
  val day2 = "MON"

  def kind_of_day(day: String): String = {
    day match {
    case "MON" | "TUE" | "WED" | "THU" | "FRI" => "weekday"
    case "SAT" | "SUN"                         => "weekend"
    case _                                     => "unknown"
    }
  }

  def handle_response(response: String): String = {
    response match {
      // pattern guard
      case s if s != null => s"Received '$s'"
      case s => "Error! Received a null response"
    }
  }

  val xx: Int = 12180
  val yy: Any = xx

  def what_type(value: Any): String = {
    value match {
      case x: String => s"'x'"
      case x: Double => f"$x%.2f"
      case x: Float  => f"$x%.2f"
      case x: Long   => s"${x}l"
      case x: Int    => s"${x}i"
    }
  }

  println(max1)
  println(max2)
  println(kind_of_day("MON"))
  println(kind_of_day("MOD"))
  println(handle_response(null))
  println(handle_response("null"))
  println(what_type(yy))
}
