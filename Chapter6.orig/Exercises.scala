object Exercises extends App {
  // exercise #1
  // Write a function literal that takes two integers and returns the higher number.
  // Then write a higher-order function that takes a 3-sized tuple of integers
  // plus this function literal, and uses it to return the maximum value in the tuple.

  val biggest = (x: Int, y: Int) => if (x > y) x else y

  def maxxer(nums: (Int, Int, Int), f: (Int, Int) => Int): Int = {
    f(f(nums._1, nums._2), nums._3)
  }

  println("exercise #1")
  println(biggest(3,4))
  println(biggest(5,4))
  println(maxxer((1,2,3), biggest))
  println(maxxer((6,7,3), biggest))

}
