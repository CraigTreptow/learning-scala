import com.craigtreptow.{RandomPoint, User, Console}

object Exercises extends App {
  val u1 = new User("Craig")
  println(u1)
  println(u1.greet)
  val p1 = new RandomPoint()
  println(s"Location is ${p1.x}")
  val p2 = new RandomPoint()
  println(s"Location is ${p2.x}, ${p2.y}")

  println("exercise #1")
  val xbox = new Console(make = "xbox")
  println(xbox)
  val ps5 = new Console(make = "xbox", debut_date_string = "2020-12-25")
  println(ps5)
}
