package com.craigtreptow

class User(val name: String) {
  def greet: String = s"Hello from $name"

  override def toString: String = s"com.craigtreptow.User($name)"
}
