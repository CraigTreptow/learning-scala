package com.craigtreptow

class Console(val make: String,
              val debut_date_string: String = "",
              val model: String = "model",
              val wifi_type: String = "N/A",
              val media_formats: List[String] = List("CD", "BluRay"),
              val max_resolution: String = "1920X1200") {

  val debut_date: java.util.Date = {
    debut_date_string match {
      case "" => java.util.Calendar.getInstance.getTime
      case s => var dd = new java.text.SimpleDateFormat("yyyy-MM-dd"); dd.parse(s)
    }

  }
  override def toString = s"$make - $debut_date - $model - $wifi_type - $media_formats - $max_resolution"
}
