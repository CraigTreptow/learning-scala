object ImplicitClasses {
  // a type-safe way to "monkey-patch" new methods and fields onto existing classes
  implicit class Hello(s: String) { def hello = s"Hello, $s" }
  def test = { println("World".hello) }
}
