class Types {
  // type aliases
  type Whole = Int
  val x: Whole = 55
  val y: Int = 66
  def test(w: Whole) = { w }
  val xx = test(x)
  val yy = test(y)
}
