object Exercises extends App {
  // exercise #1
  // Create a list of the first 20 odd Long numbers. Can you create this with a for-loop, with the filter operation,
  // and with the map operation? What’s the most efficient and expressive way to write this?

  val odds1 = List.range(1L,1_000L).filter(_ % 2 != 0).take(20)
  val odds2 = (for (i <- 1L to 1_000L if i % 2 != 0) yield i).take(20).toList
  val odds3 = List.range(1L,20L).filter(_ % 2 != 0).take(20)

  println("exercise #1")
  println(odds1)
  println(odds2)

  // exercise #2
  // Write a function titled “factors” that takes a number and returns a list of its factors,
  // other than 1 and the number itself. For example, factors(15) should return List(3, 5).
  // Then write a new function that applies “factors” to a list of numbers.
  // Try using the list of Long numbers you generated in exercise 1.
  // For example, executing this function with List(9, 11, 13, 15) should return List(3, 3, 5),

  def factors(n: Long) = {
    (for (i <- 1L to n/2 if n % i == 0) yield i).filter(_ != 1).filter(_ != n)
  }

  def doIt(l: List[Long]) = {
    l.flatMap(factors(_))
    // odds1.flatMap(factors(_))
    // odds3.flatMap(factors(_))
  }

  println("\nexercise #2")
  println(factors(15))
  println(factors(16))
  println(doIt(odds3))
  println(doIt(odds1))

  // exercise #3
  // Write a function, first[A](items: List[A], count: Int): List[A], that returns the first x number of items in a given list.
  // For example, first(List('a','t','o'), 2) should return List('a','t').
  // You could make this a one-liner by invoking one of the built-in list operations that already performs this task,
  // or (preferably) implement your own solution.
  // Can you do so with a for-loop? With foldLeft?
  // With a recursive function that only accesses head and tail?

  def first[A](items: List[A], count: Int): List[A] = {
    items.take(count)
  }

  println("\nexercise #3")
  println(first(List('a', 't', 'o'), 2))

  // exercise #4
  // Write a function that takes a list of strings and returns the longest string in the list.

  def longest(l: List[String]): String = {
    l.reduce((a,b) => if (a.length > b.length) a else b)
  }

  def longest2(l: List[String]): String = {
    l.maxBy(_.size)
  }

  println("\nexercise #4")
  println(longest(List("a", "bb")))
  println(longest(List("dddd", "a", "bb")))
  println(longest2(List("a", "bb")))
  println(longest2(List("dddd", "a", "bb")))

  // exercise #5
  // Reverse a list (roll your own code)

  def rev(l: List[Any]): List[Any] = l match {
    case Nil      => List()
    case x :: Nil => List(x)
    case x :: xs  => rev(xs) :+ x
  }

  println("\nexercise #5")
  println(rev(List()))
  println(rev(List("a", "bb")))
  println(rev(List("dddd", "a", "bb")))

  // exercise #6
  // Write a function that takes a List[String] and returns a (List[String],List[String]), a tuple of string lists.
  // The first list should be items in the original list that are palindromes (written the same forward and backward, like “racecar”).
  // The second list in the tuple should be all of the remaining items from the original list.
  // You can implement this easily with partition, but are there other operations you could use instead?

  def splitPallies(l: List[String]) = l partition (s => s == s.reverse)

  // I had to look to the the book solution above, and then I saw what I was missing.
  // I needed to add the anonymous function signature ('(a) =>') to get my solution working
  def pals(l: List[String]): (List[String], List[String]) = {
    val palindromes    = l.filter((a) => a == a.reverse)
    val nonPalindromes = l.filter((a) => a != a.reverse)
    (palindromes, nonPalindromes)
  }

  println("\nexercise #6")
  println(splitPallies(List("aa", "craig", "racecar")))
  println(pals(List("aa", "craig", "racecar")))

  // exercise #7
  // OpenWeatherMap API

  // the API has changed since the book was written.  Although sort interesting, I'm going to
  // move on with the book.
  val url = "http://api.openweathermap.org/data/2.5/forecast/hourly?mode=xml&lat=55&lon=0"
  val l: List[String] = io.Source.fromURL(url).getLines.toList

  println("\nexercise #7")
  println(l)
}
