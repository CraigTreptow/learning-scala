object Exercises extends App {
  // exercise #1
  // The Fibonacci series starts with the numbers “1, 1” and then computes each successive element as the sum of the previous two elements.
  // We’ll use this series to get familiarized with the collections in this chapter.
  //
  // a. Write a function that returns a list of the first x elements in the Fibonacci series
  // b. Write a new Fibonacci function that adds new Fibonacci numbers to an existing list of numbers.
  //    It should take a list of numbers (List[Int]) and the count of new elements to add and return a new list (List[Int]).
  // Stream is deprecated now

  def fibA(a: Int, b: Int): LazyList[Int] = LazyList.cons(a, fibA(b, a + b))

//  def fibB(l: List[Int], c: Int): List[Int] = {
//    val b = l.toBuffer
//    b += fibA(1,1).take(c).toList
//    b.toList
//  }

  def fibNext(n: Int) = {
    val f = fibA(1,1).take(n + 1).toList
    f(n)
  }

  println("exercise #1")
  println(fibA(1,1).take(10).toList)
  // println(fibB(List(9,8,7), 9))
  println(fibNext(8))

  // exercise #4
  // Write a function to return the product of two numbers that are each specified as a String, not a numeric type.

  def product(a: String, b: String): Option[Int] = {
    def parseIt(x: String) = util.Try(x.toInt).toOption

    val parseResults = (parseIt(a), parseIt(b))

    parseResults match {
      case (Some(x), Some(y)) => Some(x * y)
      case (_, _) => None
    }
  }

  println("\nexercise #4")
  println(product("1","3"))
  println(product("1","three"))

  // exercise #5
  // Write a function to safely wrap calls to the JVM library method System.getProperty(<String>), avoiding raised exceptions or null results.

  def systemX(s: String): String = {
    System.getProperty(s) match {
      case null => "N/A"
      case s    => s
    }
  }

  println("\nexercise #5")
  println(systemX("java.home"))
  println(systemX("three"))

}
